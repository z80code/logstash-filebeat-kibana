import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Main {

	private static final Logger logger = LogManager.getLogger(Main.class);

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Starting application.");
		// start log
		logger.info("Starting application.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.trace("It`s TRACE log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.debug("It`s DEBUG log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.info("It`s INFO log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.warn("It`s WARN log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.error("It`s ERROR log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.fatal("It`s FATAL log level message.");
		TimeUnit.MILLISECONDS.sleep(300);
		logger.info("Exiting application.");
		// end log
		System.out.println("Exiting application.");
	}
}
